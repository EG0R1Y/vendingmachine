﻿namespace VendingMachine.Model
{
    class Product
    {
        public string Name { get; set; }
        public int Cost { get; set; }
        public string NumberPortion { get; set; }
    }
}
