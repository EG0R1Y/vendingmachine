﻿using System.Threading.Tasks;

namespace VendingMachine.Model
{
    public interface IDataService
    {
        Task<DataItem> GetData();
    }
}